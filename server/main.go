package main

import (
    "context"
    "fmt"
    api "gitlab.com/qwert7777776/SOAmafia/api"
    "gitlab.com/qwert7777776/SOAmafia/server/config"
    "google.golang.org/grpc"
    "google.golang.org/grpc/codes"
    "google.golang.org/grpc/metadata"
    "google.golang.org/grpc/status"
    "log"
    "math/rand"
    "net"
    "sync"
    "time"
)

const tokenHeader = "token"

var Roles = map[int]string{
    0: "Townie",
    1: "Doctor",
    2: "Inspector",
    3: "Mafia",
}

type MafiaServer struct {
    api.UnimplementedMafiaServer
    m                    sync.RWMutex
    TokenToUser          map[string]string
    BroadcastMessage     chan *api.Response
    UserStream           map[string]chan *api.Response
    CurrentUserStream    map[string]chan *api.Response
    Config               *config.Config
    PlayersInWaitingRoom int
    CurrentQueue         []*Player
    CurrentGameNumber    int
    UsersService         map[string]api.Mafia_PlayServer
}

type Player struct {
    Token string
    Name  string
    Srv   api.Mafia_PlayServer
}

func NewPlayer(token string, name string, srv api.Mafia_PlayServer) *Player {
    return &Player{
        Token: token,
        Name:  name,
        Srv:   srv,
    }
}

func NewMafiaServer() *MafiaServer {
    return &MafiaServer{
        m:                    sync.RWMutex{},
        TokenToUser:          make(map[string]string),
        BroadcastMessage:     make(chan *api.Response, 10),
        UserStream:           make(map[string]chan *api.Response),
        CurrentUserStream:    make(map[string]chan *api.Response),
        Config:               &config.Config{PlayersToStart: 2},
        PlayersInWaitingRoom: 0,
        CurrentQueue:         make([]*Player, 0),
        CurrentGameNumber:    0,
        UsersService:         make(map[string]api.Mafia_PlayServer),
    }
}
func main() {
    log.Println("Server running ...")

    lis, err := net.Listen("tcp", ":8000")
    if err != nil {
        log.Fatalln(err)
    }

    srv := grpc.NewServer()
    m := NewMafiaServer()
    api.RegisterMafiaServer(srv, m)
    go m.broadcast()

    log.Fatalln(srv.Serve(lis))
}

func (s *MafiaServer) extractToken(ctx context.Context) (tkn string, ok bool) {
    md, ok := metadata.FromIncomingContext(ctx)
    if !ok || len(md[tokenHeader]) == 0 {
        return "", false
    }

    return md[tokenHeader][0], true
}

func (m *MafiaServer) Play(srv api.Mafia_PlayServer) error {
    tkn, ok := m.extractToken(srv.Context())
    if !ok {
        return status.Error(codes.Unauthenticated, "missing token header")
    }
    player := NewPlayer(tkn, m.TokenToUser[tkn], srv)
    go m.sendBroadcasts(srv, player)

    <-srv.Context().Done()
    return srv.Context().Err()
}
func (m *MafiaServer) sendBroadcasts(srv api.Mafia_PlayServer, player *Player) {
    stream := m.registerPlayer(srv, player)
    defer m.deletePlayer(srv, player)
    for {
        select {
        case <-srv.Context().Done():
            return
        case res := <-stream:
            if s, ok := status.FromError(srv.Send(res)); ok {
                switch s.Code() {
                case codes.OK:
                    continue
                case codes.Unavailable, codes.Canceled, codes.DeadlineExceeded:
                    log.Printf("canceled: %s \n", player.Token)
                    return
                default:
                    log.Printf("failed: %s \n", player.Token)
                    return
                }
            }
        }
    }
}

func (m *MafiaServer) registerPlayer(srv api.Mafia_PlayServer, player *Player) chan *api.Response {
    m.m.Lock()
    stream := make(chan *api.Response, 1000)
    m.UserStream[player.Token] = stream
    m.CurrentUserStream[player.Token] = stream
    m.UsersService[player.Token] = srv
    m.BroadcastMessage <- &api.Response{Message: fmt.Sprintf("player %s CONNECTED \n", m.TokenToUser[player.Token])}
    m.PlayersInWaitingRoom += 1
    m.BroadcastMessage <- &api.Response{Message: fmt.Sprintf("%d players to start\n", m.Config.PlayersToStart-m.PlayersInWaitingRoom)}
    m.CurrentQueue = append(m.CurrentQueue, player)

    if m.PlayersInWaitingRoom >= m.Config.PlayersToStart {
        g := NewGame(m.CurrentQueue, m.CurrentGameNumber, m.CurrentUserStream, m.UsersService)
        m.UsersService = make(map[string]api.Mafia_PlayServer)
        m.CurrentQueue = make([]*Player, 0, m.Config.PlayersToStart)
        m.CurrentUserStream = make(map[string]chan *api.Response)
        for k, _ := range m.CurrentUserStream {
            delete(m.UserStream, k)
        }
        go g.PlayGame()
        //m.BroadcastMessage <- &api.Response{Message: fmt.Sprintf("Game started")}
        m.CurrentGameNumber += 1
    }
    m.m.Unlock()
    return stream
}
func (m *MafiaServer) deletePlayer(srv api.Mafia_PlayServer, player *Player) {
    m.m.Lock()
    delete(m.UserStream, player.Token)
    m.BroadcastMessage <- &api.Response{Message: fmt.Sprintf("player %s DISCONNECTED \n", m.TokenToUser[player.Token])}
    m.m.Unlock()
}
func (m *MafiaServer) getName(token string) string {
    m.m.Lock()
    defer m.m.Unlock()
    return m.TokenToUser[token]
}

func (m *MafiaServer) broadcast() {
    for res := range m.BroadcastMessage {
        m.m.RLock()
        for _, stream := range m.UserStream {
            select {
            case stream <- res:
                continue
            default:
                log.Printf("err sending to smth \n")
                return
            }
        }
        m.m.RUnlock()
    }
}

func NewAuthToken(username, password string) string {
    tkn := make([]byte, 4)
    rand.Read(tkn)
    return fmt.Sprintf("%x", tkn)
}

func (m *MafiaServer) Login(ctx context.Context, request *api.LoginRequest) (*api.LoginResponse, error) {
    token := NewAuthToken(request.Username, request.Password)
    m.TokenToUser[token] = request.Username
    return &api.LoginResponse{Token: token}, nil
}

type Game struct {
    Id               int
    Players          []*Player
    GameUserStream   map[string]chan *api.Response
    BroadcastMessage chan *api.Response
    m                sync.RWMutex
    UsersService     map[string]api.Mafia_PlayServer
    NumberMafia      int
    NumberComissar   int
    NumberDoctor     int
    NumberWhite      int
    UserStatus       map[string]int
}

func NewGame(players []*Player, id int, stream map[string]chan *api.Response, userSrv map[string]api.Mafia_PlayServer) *Game {
    return &Game{
        Id:               id,
        Players:          players,
        GameUserStream:   stream,
        BroadcastMessage: make(chan *api.Response, 10),
        m:                sync.RWMutex{},
        UsersService:     userSrv,
        NumberMafia:      3,
        NumberComissar:   1,
        NumberDoctor:     1,
        NumberWhite:      6,
        UserStatus:       make(map[string]int),
    }
}
func (g *Game) gameBroadcast() {
    for res := range g.BroadcastMessage {
        g.m.RLock()
        for _, stream := range g.GameUserStream {
            select {
            case stream <- res:
                continue
            default:
                log.Printf("err sending to smth \n")
                return
            }
        }
        g.m.RUnlock()
    }
}

func (g *Game) GameRoles() {
    // задаем слайс с элементами
    s := []int{3, 3, 3, 1, 2, 0, 0, 0, 0, 0}

    rand.Seed(time.Now().UnixNano())

    // применяем алгоритм перетасовки Фишера-Йетса
    for i := len(s) - 1; i > 0; i-- {
        j := rand.Intn(i + 1)
        s[i], s[j] = s[j], s[i]
    }
    i := 0
    for k := range g.UserStatus {
        g.UserStatus[k] = s[i]
        g.GameUserStream[k] <- &api.Response{Message: fmt.Sprintf("you are %s", Roles[s[i]])}
        //g.UsersService[k].Send(&api.Response{Message: fmt.Sprintf("you are %s", Roles[s[i]])})
        i++
    }
}

func (g *Game) Election() bool {
    return true
}

func (g *Game) MafiaKill() bool {
    return false
}

func (g *Game) DoctorTreat() {

}

func (g *Game) Komissar() {

}

func (g *Game) FinishGame() {
    g.BroadcastMessage <- &api.Response{Message: "end game"}
}
func (g *Game) PlayGame() {
    for _, v := range g.Players {
        g.UserStatus[v.Token] = 0
    }
    go g.gameBroadcast()

    g.BroadcastMessage <- &api.Response{Message: "start game"}
    time.Sleep(time.Second / 10)
    g.GameRoles()
    for {
        if g.Election() {
            break
        }

    }

    time.Sleep(time.Second * 10)
    g.FinishGame()
}
