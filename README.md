# SOAmafia


## Start
```
sudo docker compose up
```
## Start client
```
 go run client/main.go 
```

Для простоты проверки игра ничинается когда подсоединяются два человека, игровая логика не реализована
```
Примерный план игры со стороны Ильи выглядит так

Your username:
ilya
2023-06-11 23:53:47.534943895 +0300 MSK m=+2.154003866 logged in successfully
connect to stream
message:"player ilya CONNECTED \n"
message:"1 players to start\n"
message:"player bob CONNECTED \n"
message:"0 players to start\n"
message:"start game"
message:"you are Mafia"
message:"end game"
```