FROM golang:1.18-alpine


RUN mkdir /service
WORKDIR /service

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build server/main.go

EXPOSE 8000

CMD ["./main"]
