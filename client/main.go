package main

import (
    "fmt"
    "time"

    chat "gitlab.com/qwert7777776/SOAmafia/api"
    "golang.org/x/net/context"
    "google.golang.org/grpc"
    "google.golang.org/grpc/metadata"
)

func main() {
    var username string
    fmt.Println("Your username:")
    fmt.Scanf("%s\n", &username)
    c := NewClient("localhost:8000", "kek", username)
    fmt.Println(c.Run(context.Background()))
}

type Client struct {
    chat.MafiaClient
    Host, Password, Username, Token string
    Shutdown                        bool
}

func NewClient(host, pass, name string) *Client {
    return &Client{
        Host:     host,
        Password: "kek",
        Username: name,
    }
}

func (c *Client) Run(ctx context.Context) error {
    connCtx, cancel := context.WithTimeout(ctx, time.Second)
    defer cancel()
    conn, err := grpc.DialContext(connCtx, c.Host, grpc.WithInsecure(), grpc.WithBlock())
    if err != nil {
        return fmt.Errorf("unable to connect\n")
    }
    defer conn.Close()

    c.MafiaClient = chat.NewMafiaClient(conn)

    if c.Token, err = c.login(ctx); err != nil {
        return fmt.Errorf("%s\n", err.Error())
    }
    fmt.Println(time.Now(), "logged in successfully")

    err = c.stream(ctx)

    fmt.Println(time.Now(), "logging out")
    return nil
}

func (c *Client) stream(ctx context.Context) error {
    md := metadata.New(map[string]string{"token": c.Token})
    ctx = metadata.NewOutgoingContext(ctx, md)

    ctx, cancel := context.WithCancel(ctx)
    defer cancel()

    client, err := c.Play(ctx)
    if err != nil {
        return err
    }
    defer client.CloseSend()

    fmt.Println("connect to stream")

    go c.send(client)
    return c.receive(client)
}

func (c *Client) receive(sc chat.Mafia_PlayClient) error {
    for {
        res, err := sc.Recv()
        if err != nil {
            fmt.Println(err)
            return nil
        }
        fmt.Println(res)
    }
}

func (c *Client) send(client chat.Mafia_PlayClient) {

    for {
        select {
        case <-client.Context().Done():
        default:
            var x int32
            _, err := fmt.Scanf("%d", &x)
            if err != nil {
                continue
            }
            if err := client.Send(&chat.Request{Message: x}); err != nil {
                fmt.Println(err)
                return
            } else {
                return
            }
        }
    }
}

func (c *Client) login(ctx context.Context) (string, error) {
    request := &chat.LoginRequest{
        Username: c.Username,
        Password: c.Password,
    }
    res, err := c.MafiaClient.Login(ctx, request)

    if err != nil {
        return "", err
    }

    return res.Token, nil
}
